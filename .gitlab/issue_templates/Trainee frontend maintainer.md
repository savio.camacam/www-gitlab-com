## Basic setup

1. [ ] Read the [code review page in the handbook](https://about.gitlab.com/handbook/engineering/workflow/code-review/) and the [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
2. [ ] Understand [how to become a maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-maintainer) and add yourself as a [trainee maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer) on the [team page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml).

## Working towards becoming a maintainer

There is no checklist here, only guidelines. Remember that there is no specific timeline on this.

It is up to you to ensure that you are getting enough MRs to review, and of
varied types. All engineers are reviewers, so you should already be receiving
regular reviews from Reviewer Roulette. You could also seek out more reviews
from your team, or #frontend Slack channels.

Your reviews should aim to cover maintainer responsibilities as well as reviewer
responsibilities. Your approval means you think it is ready to merge.

We recommend to act as a coach in a big deliverable that requires [following the planning step](https://docs.gitlab.com/ee/development/fe_guide/development_process.html#planning-development) as part of the trainee program.

After each MR is merged or closed, add a discussion to this issue using this
template:

```markdown
### (Merge request title): (Merge request URL)

During review:

- (List anything of note, or a quick summary. "I suggested/identified/noted...")

Post-review:

- (List anything of note, or a quick summary. "I missed..." or "Merged as-is")

(Maintainer who reviewed this merge request) Please add feedback, and compare
this review to the average maintainer review.
```

**Note:** Do not include reviews of security MRs because review feedback might
reveal security issue details.

## When you're ready to make it official

When reviews have accumulated, and recent reviews consistently fulfill
maintainer responsibilities, any maintainer can take the next step. The trainee
should also feel free to discuss their progress with their manager or any
maintainer at any time.

1. [ ] Create a merge request for [team page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml) proposing yourself as a maintainer.
2. [ ] Create a merge request for [CODEOWNERS](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab/CODEOWNERS), adding yourself accordingly, and ask a maintainer to review it.
4. [ ] Keep reviewing, start merging :metal:

/label ~"trainee maintainer"
