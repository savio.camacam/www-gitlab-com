---
layout: markdown_page
title: "Hybrid-Remote"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction 

On this page, we're detailing the primary differences between all-remote and other forms of remote working.

## What is hybrid-remote?

Hybrid-remote (which can be referred to as part-remote), is [different than all-remote](/2018/10/18/the-case-for-all-remote-companies/). In an all-remote company, there is no single headquarters, and each team member is free to live and work in any place they choose. Everyone, including executives, is remote, as there are no offices to come to. 

Hybrid-remote is currently more common than all-remote, as it is easier for large, established companies to implement. In a hybrid-remote scenario, there is one or more offices where a subset of the company commutes to each day — working physically in the same space — paired with a subset of the company that works remotely. 

These institutions are primarily colocated, but allow remote work. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/QFbrPR0jAYs?start=3" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, GitLab co-founder and CEO Sid Sijbrandij chats with Maren Kate, founder at AVRA Talent Partners.* 

In discussing the decision to go all-remote at GitLab, Sid shares the following.

> For us, it was really important that people didn't have to come to the office to get information necessary for career opportunities. 
>
> From very early on, we started writing things down. During Y Combinator, they told us "Look, remote work is for engineering, but not finance, marketing, or sales."
>
> So, we got an office. People got hired, they came there, **but after a few days they stopped showing up**. 
>
> [Coming to the office] wasn't needed. They weren't getting any extra information. They were on Slack, on Zoom, in Google Docs, in GitLab pages, in GitLab Issues, in GitLab merge requests — they didn't need to be there. 
>
> **It's not that people like their commute; it's just that people don't want to miss out.** If you make sure that people don't miss out, you can be remote, too. It takes a lot of effort and focus to make sure all conversations are captured appropriately and that everything is documented. - *GitLab co-founder and CEO Sid Sijbrandij*

## Do hybrid-remote employees have a commute?

Some hybrid-remote arrangements do involve *regular* commutes to the office, though not *daily* commutes. For example, a remote employee in a hybrid-remote organization may travel to an office one week each month for regularly scheduled in-person interactions, while working from a location of their choosing the rest of the month.

While this scenario may still be preferred over one where remote employees are *not* invited to visit in-person offices, it isn't quite as flexibile as all-remote. There's still a commute involved, which can take the majority of a day in both directions for commutes involving flights. 

## Are there advantages to hybrid-remote?

hybrid-remote arrangements such as the above offer unique advantages. For hybrid-remote employees who can count on a regular trip to a destination funded by their employer, they're able to [plan micro-trips around their business travel](https://thepointsguy.com/guide/best-jobs-for-traveling/). 

For hybrid-remote employees with a taste for exploration and a flexibile schedule, these built-in business trips can serve as [jumping-off points](https://thepointsguy.com/guide/maximize-business-travel/) for exploring new locales that they may not have the means to explore in an all-remote company.

For employers who are committed to a colocated model, but wish to expand their recruiting pipeline beyond the city where they are headquartered, allowing remote employees to join their ranks can be beneficial. Employers may be able to find exceptional team members in a more [diverse array of locales](/company/culture/all-remote/hiring/index.html), pay them [local rates](/2019/02/28/why-we-pay-local-rates/), and sidestep ongoing talent wars in major metropolitan areas. 

In doing so, employers would effectively enact a hybrid-remote model, which requires additional considerations to prevent remote employees from being significantly disadvantaged — a point we'll cover in detail below.

## Disadvantages to hybrid-remote

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/IFBj9KQSQXA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, GitLab co-founder and CEO Sid Sijbrandij chats with InVision Chief People Officer Mark Frein on the future of all-remote. In the [conversation](/2019/07/31/pyb-all-remote-mark-frein/), the two discuss the differences between all-remote and hybrid-remote.*

All things being equal, employees longing for additional freedom, autonomy, and workplace flexibility will likely view a hybrid-remote arrangement as superior to a colocated arrangement — one which *requires* a commute and an in-person presence on a daily basis. 

Said another way, "some remote" is often viewed as superior to "no remote." Though far from ideal, it can be rationalized that fewer career opportunities, added judgement, and difficulties in bonding are prices worth paying to live and work where one wants.

There are considerations when accepting a role in a hybrid-remote company, and it's important to be mindful of these potential downsides.

1. **Hybrid-remote employees may have less access to information**. Unless you work for an employer that [documents everything](/handbook/values/#write-things-down), you may be asked to handle your day-to-day duties with less information — and incomplete information — compared to in-person colleagues. Over time, this can lead to more mistakes, confusion, frustration, and even [underperformance](/handbook/underperformance/). 
1. **Fewer career and development opportunities**. Hybrid-remote employees who are out of sight may be [passed over](https://hbr.org/2017/11/a-study-of-1100-employees-found-that-remote-workers-feel-shunned-and-left-out) for promotions, advancement, and development [opportunities](https://news.ycombinator.com/item?id=15994294). They may also have fewer opportunities to more horizontally within the organization, and less influence to create a new role to serve evolving business needs.
1. **The feeling of being a satellite office**. Hybrid-remote employees must put effort towards not being treated as less-than within the organization. It is important to surface relevant inquiries during the interview process as to how remote colleagues are [onboarded](/company/culture/all-remote/learning-and-development/), included, and perceived by others. Some employees may not be fazed by this treatment, but it can take a mental and emotional toll on others.  
1. **Managing guilt**. It is not uncommon to hear remote workers [express guilt](https://www.glassdoor.com/blog/work-from-home-guilt/) if they work in a company which is primarily colocated. Their socializing involves colleagues who may complain about commutes, or express sadness due to an inability to attend a certain family function. There are inherit inequalities in this arrangement, requiring the remote employee to emphathize with in-person colleagues despite not being required to endure the same commutes and inflexibility. 
1. **The burden of lobbying for remote**. If an employee is brought on in a remote capacity, but this arrangement is not supported equally across teams and managers, a situation may arise where the remote employee is constantly justifying the perceived privilege of not being forced to commute into a physical office. 
1. **Determining whether remote is truly offered and supported**. Many large companies will tolerate remote employees, but they will not openly advertise roles as remote, nor will they publicly admit that they support remote work. This creates an exhausting game of hide-and-seek when searching for roles, in addition to searching for remote-friendly managers and teams *within* such an organization.
1. **Risk of being made an example of**. It is possible for remote employees in a primarily colocated company to be asked questions like "So, how did you finagle a remote arrangement?" This places remote employees in a difficult situation. Either they choose to champion the cause of empowering even more colleagues to work remotely, potentially harming their reputation in the process, or they appear unhelpful by keeping the perceived perk to themselves. 
1. **Demands for overperformance**. When you're a remote employee working with colleagues who endure long commutes each day, you may encounter pressure — however subtle — to deliver results beyond those expected of in-person team members. This stems from a toxic culture of envy, where colocated employees deduce that if they must endure inflexibility and commutes, remote colleagues must produce additional results as to not get off easier. 

## Differences between all-remote and remote-first

In a remote-first organization, the *default* is remote. While there may be a company headquarters, or even an array of satellite offices, everything from how meetings are handled to onboarding new hires is [structured through a remote lens](https://stackoverflow.blog/2017/02/08/means-remote-first-company/).

Said another way, there is a culture of assuming that remote is the norm, not the exception, and processes are established to reinforce that.

If you're considering working for a remote-first company, consider asking the following.

1. Do you have a headquarters?
1. Does the executive team work remotely, or do they all congregate in the same physical space day-to-day?
1. Where and how are all-hands calls handled?
1. Are interviews and onboarding handled on-premise or via video call?
1. Do team members have [hybrid calls](/handbook/communication/#hybrid-calls-are-horrible), or does each person have their own equipment?
1. What procedures are in place to ensure that remote employees are granted [learning and development](/company/culture/all-remote/learning-and-development/) and promotion opportunities? 

It is possible to find remote-first companies with a healthy, understanding culture that works to support both colocated and remote colleagues. Prospective employees should do their own due diligence to make sure the requisite values are established and lived out. 

----

Return to the main [all-remote page](/company/culture/all-remote/).
