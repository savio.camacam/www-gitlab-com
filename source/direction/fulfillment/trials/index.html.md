---
layout: markdown_page
title: "Category Vision - Trials"
---

- TOC
{:toc}

## 🤔 Trials

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas,
and user journeys into this section. -->

* [Overall Vision](/direction/fulfillment)
* [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=trials%20%F0%9F%A4%94)
* [Maturity: Viable](/direction/maturity)
* [Documentation]()
* [All Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=trials%20%F0%9F%A4%94)

The Trials category covers the trial experience for GitLab.com (SaaS) customers and Self-Managed customers, from sign up to the last day of the trial.

Please reach out to  Luca Williams, Product Manager for Fulfillment ([E-Mail](mailto:luca@gitlab.com)/
[Twitter](https://twitter.com/tipyn2903)) if you'd like to provide feedback or ask
questions about what's coming.

## 🔭 Vision

## 🎭 Target audience and experience
<!-- An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

For more information on how we use personas and roles at GitLab, please [click here](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/).

## 🚀 What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

#### Current focus

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 1️⃣ | [An improved free trial sign-up experience for GitLab.com (SaaS) users](https://gitlab.com/gitlab-org/gitlab-ee/issues/13233) | A free trial is often the first interaction our users have with our Product and therefore could be a dealbreaker for them. Currently, the free trial experience as a whole is confusing,lengthy and frustrating and this issue to improve the sign-up flow of our GitLab.com free trial is our first iteration towards creating a delightful and exciting first-look experience into GitLab as a product. |

#### Next up

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 2️⃣ |  |
| 3️⃣ |  |
| 4️⃣ |  | 

Please see the [Fulfillment planning board](https://gitlab.com/groups/gitlab-org/-/boards/928182?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Afulfillment) for more details on what's next for Transactions.

## 🏅 Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

## 🔬 Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## ❤️ Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## 🎢 Top user issues
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## 🦊 Top internal customer issues/epics
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

## 🔭 Top Vision Item(s)
<!--  What's the most important thing to move your vision forward?-->
