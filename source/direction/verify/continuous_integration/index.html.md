---
layout: markdown_page
title: "Category Vision - Continuous Integration"
---

- TOC
{:toc}

## Continuous Integration

Many teams report that CI, while important and commonly adopted, is too slow and teams are unsure how to speed it up. It's often a black box that few people understand and diving in feels like an interruption to the more important work of delivering features. This presents a unique opportunity for CI solutions that make optimization of the pipeline easier and more automatic.

While we are very proud that GitLab CI/CD was recognized as [the leading CI/CD tool on the market](/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/), and has been called `lovable` by many, many users - we still have a vision for how we can make what we've built even better.  In other epics, we talk about [Speed & Scalability](https://gitlab.com/groups/gitlab-org/-/epics/786) and [making CI lovable for every use case](https://gitlab.com/groups/gitlab-org/-/epics/811).  This vision epic will cover the broadest area of CI in GitLab - and how we're going to go from `lovable` to `undeniably amazing`.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Integration)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1304) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

### Continuous Integration vs. Delivery vs. Deployment

CI and CD are very closely related areas at GitLab: our CI platform (what we call Verify) provides the foundation for CD and is the [Gateway to Operations](/direction/verify/#gateway-to-operations).  In fact, Verify is also the first stage in our [CI/CD section](/handbook/product/categories/#cicd-section) which also includes Package and Release.  In addition to the vision for each of those stages, we maintain a section wide [vision for CI/CD](/direction/cicd/)

### GitLab Runner

The GitLab Runner is managed by the Verify team also, and is a key component of the CI solution. It has its own strategy that can be found on its [direction page](/direction/verify/runner/)

### Monorepos and Cross-Project Pipelines

There are two important types of pipeline use-cases that we're always keeping in mind as we're building features.
Monorepos are where you put all of your projects into a single repository, and cross-project piplelines (taken
to an extreme) are where you might put every service in a microservice architecture into different repositories.
There are pros and cons to each approach, but we want to be sure GitLab effectively supports both by providing
features that are needed for one or the other, or support both.

A few of our popular items for supporting cross-project pipelines better are:

- [Segregate access control regarding pipelines from push/merge](https://gitlab.com/gitlab-org/gitlab-ce/issues/52374)
- [Report commit status based on deterministic pipeline groups](https://gitlab.com/gitlab-org/gitlab-ce/issues/25734)
- [Use multi-project pipelines with CE/EE and Omnibus](https://gitlab.com/gitlab-org/gitlab-qa/issues/63)
- [Cross-project triggered-by registry image change](https://gitlab.com/gitlab-org/gitlab-ee/issues/10074)

And for monorepos:

- [Child/parent pipelines](https://gitlab.com/gitlab-org/gitlab-ce/issues/22972)
- [Make it possible to use any language to generate .gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-ce/issues/45828)

In support of both, we recently introduced the Directed Acyclic Graph. You can read more about the follow-ups
we have planned in the epic [gitlab-org&1716](https://gitlab.com/groups/gitlab-org/-/epics/1716).

## What's Next & Why

Up next is implementing a Directed Acyclic Graph keyword (`needs:`) to allow for out-of-sequence execution of jobs, disregarding stages:

* [Directed acyclic graphs (DAG) for pipelines MVC](https://gitlab.com/gitlab-org/gitlab-ce/issues/47063)

The DAG opens up new possibilities for building pipelines within GitLab, including for monorepos or data science, and Vault integration adds to the security of GitLab CI pipelines.

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our
[definitions of maturity levels](/direction/maturity/)),
we do not have an upcoming target for bringing it to the next level. It's important to us that
we defend our lovable status, though, so as you see any actual or potential gap please let us know in
our [public epic](https://gitlab.com/groups/gitlab-org/-/epics/1307) for this category. The most important
item right now to maintain lovable maturity is multi-platform shared runners ([gitlab-com&42](https://gitlab.com/groups/gitlab-com/-/epics/42)).

## Competitive Landscape

In general, to be more competitive we are considering [gitlab-ee#10107](https://gitlab.com/gitlab-org/gitlab-ee/issues/10107) to move GitLab CI/CD for External Repos (GitHub + BitBucket) from Premium to Starter. This will help more teams have access to GitLab CI/CD, and is potentially a great way to introduce more people to GitLab in general, but also comes with costs of running the CI/CD so the decision needs to be weighed closely against the opportunity.

### Azure DevOps/GitHub

Microsoft is planning to use their GitHub acquisition to pick up market share in going after open source software development for their new integrated solution. There had been a trend to move away from them because users saw their current development tools as old-fashioned; with recently rebranding them to Azure DevOps (and investment in improving their capabilities) they are trying to disrupt this decision. We need to ensure that our support for Microsoft stacks is high quality, and issues like [gitlab-ce#61970](https://gitlab.com/gitlab-org/gitlab-ce/issues/61970) will help us achieve that by making sure the experience using GitLab for .NET development is just as good as on Azure DevOps. GitHub CI/CD, although being kept separate as a product and infrastructure from Azure DevOps, leverages the same codebase for its implementation.

#### Multi-platform runners

The most urgent gap that we want to address to compete here, though, is the capability to have Windows and MacOS runners as part of our shared fleet. It's already possible to use runners with these operating systems, but it requires you to bring your own which is a definite barrier to getting up and running quickly. We are tracking the infrastructure deliverables we need to achieve this goal in our epic [gitlab-com&42](https://gitlab.com/groups/gitlab-com/-/epics/42), along with the gitlab-ce issue [gitlab-ce#65824](https://gitlab.com/gitlab-org/gitlab-ce/issues/65824). ARM is tracked at [gitlab-runner#2076](https://gitlab.com/gitlab-org/gitlab-runner/issues/2076).

#### GitHub CI/CD

GitHub is evolving Actions into more and more of a standalone CI/CD solution. GitLab is far ahead in a lot of areas of CI/CD that they are going to have to catch up on, but Microsoft and GitHub have a lot of resources and have a big user base ready and excited to use their free product. Making an enterprise-ready solution will take some time, at least six months to a year, but they are no doubt actively working on closing these gaps.

As already [mentioned above](index.html#multi-platform-runners), multi-platform runners is our top priority, so I won't cover that again here.

Actions itself is still early days. At first blush it's nice that there are small, individually contributed pluggable modules that can do different things. There's some question, though, as to how this will scale over the medium term. The semi-distributed, event-based interaction model poses some risk of emergent pipeline behaviors becoming difficult for a human to understand, but does provide some nice flexibility in terms of relating how different events in the system can trigger different responses. Similarly, it is yet to be seen how they will avoid the Jenkins issue where many different behaviors (plugins as analog to actions) will be maintained over time (or not) by the enterprises who depend on them, and how these will be compatible with each other as complexity increases over time. There are also potential security concerns with maliciously controlled actions running code as part of your pipelines; in ecosystems where using lots of small packages from different authors is normalized (i.e., NPM) auditing and scanning to mitigate this can become burdensome.

In the short term, our plan is to make sure that anything that's easy to do using a GitHub Action is also easy to do in GitLab through a template, keyword, or other approach, and is well documented, easy to find, and just as easy to get started with. In the future, we could consider (as with CodeFresh) making GitLab able to run GitHub actions (or any other similarly Dockerized module) right inline with the pipeline. It's possible that, given a hybrid approach, we could address some of the risks around complex emergent behaviors. We definitely recognize the power and leverage that comes from being able to grab an a already-written bit of code to get up and running quickly, especially for common solutions such as normal deployments to various cloud providers. Our intial approach for this is being worked in [gitlab-ce#53307](https://gitlab.com/gitlab-org/gitlab-ce/issues/53307).

They are planning to bundle in secrets management, and we are also exploring this in our [Secrets Management category](/direction/release/secrets_management). We also have a [Package Management category](/direction/package/package_management) which is analogous to GitHub Packages.

#### Azure DevOps

Microsoft has also provided a [feature comparison between Azure DevOps (though not GitHub CI/CD) and GitLab](https://docs.microsoft.com/en-us/azure/devops/learn/compare/azure-devops-vs-gitlab) which, although it is not 100% accurate, highlights the areas where they see an advantage over us. Our responses overall as well as to the inaccuracies can be found in [our comparison page](azure_devops_detail.html).

### CloudBees Jenkins/CodeShip

Jenkins has been for a while the punching bag for how you sell DevOps software (just saying "my product solves the same problems as Jenkins, but without the huge expensive legacy mess" have given you the keys to the kingdom.) Jenkins is trying to address this by [splitting](https://jenkins.io/blog/2018/08/31/shifting-gears/) their product into multiple offerings, giving them the freedom to shed some technical and product debt and try to innovate again. They also acquired CodeShip, a SaaS solution for CI/CD so that they can play in the SaaS space. All of this creates a complicated message that doesn't seem to be resonating with analysts or customers yet.

At the moment there isn't a lot that needs to be done here to maintain our lead over the product, but continuing to add product depth (that does not add complexity) via features like [gitlab-ce#`17081`](https://gitlab.com/gitlab-org/gitlab-ce/issues/17081) will allow us to clearly demonstrate that our product is as mature as Jenkins but without the headache.

### Bitbucket Pipelines and Pipes
BitBucket pipelines has been Atlassian's answer to a more CI/CD approach than the traditional Atlassian Bamboo that is very UI driven builds and deploys by allowing users to create yml based builds.

On February 28, 2019, Atlassian announced [Bitbucket Pipes](https://bitbucket.org/blog/meet-bitbucket-pipes-30-ways-to-automate-your-ci-cd-pipeline) as an evolution of Bitbucket pipelines and gained a lot of press around [Atlassian taking on GitHub/Microsoft](https://www.businessinsider.com/atlassian-bitbucket-pipes-ci-cd-gitlab-github-2019-2).  While it is early in the evolution of this as a product in the enterprise market, there are a number of interesting patterns in the announcement that favor Convention over Configuration.  The feature that most closely matches this in our roadmap is [gitlab-ce#53307](https://gitlab.com/gitlab-org/gitlab-ce/issues/53307).

### CodeFresh

CodeFresh follows a similar [container-based plugin model](https://steps.codefresh.io/) as GitHub Actions, so our approach is similar to the one written above in response to that solution.

## Analyst Landscape

There are a few key findings from the Forrester Research analysts on our CI solution:

- GitLab is seen as the best end to end leader where other products are not keeping up and not providing a comprehensive solution. We should continue to build a deep solution here in order to stay ahead of competitor's solutions.
- Competitors claim that GitLab can go down and doesn't scale, which is perhaps their best argument if their products do not provide as comprehensive solutions. This isn't really true, so we need to have messaging and product features that make this clear.
- In general, cloud adoption of CI/CD is growing and our CI needs to be ready.

To continue to drive in this area, we are considering [gitlab-ce#40720](https://gitlab.com/gitlab-org/gitlab-ce/issues/40720) next to add Vault integration throughout the CI pipeline. This builds off of work happening on the Configure team and will allow for a more mature delivery approach that takes advantage of ephemeral credentials to ensure a rock solid secure pipeline.

## Top Customer Success/Sales Issue(s)

The most popular Customer Success issues as determined in FQ1-20 survey of the Technical Account Managers were:

* CI Views - specifically for [JUnit XML results](https://gitlab.com/gitlab-org/gitlab-ce/issues/17081)
* [`include:` keyword not expanding variables](https://gitlab.com/gitlab-org/gitlab-ce/issues/52972)
* [Filter pipelines by status or branch](https://gitlab.com/gitlab-org/gitlab-ce/issues/18054)

## Top Customer Issue(s)

The most popular customer facing issue is integration with [Hashicorp Vault](https://www.vaultproject.io/) as part of [gitlab-org#816](https://gitlab.com/groups/gitlab-org/-/epics/816)

## Top Internal Customer Issue(s)

The issue about `triggered-by` functionality is currently the top internal customer issue: [gitlab-ee#9045](https://gitlab.com/gitlab-org/gitlab-ee/issues/9045).  Other top internal customer issues include:

* Showing a high visiblity alert if master is red [gitlab-ee#10216](https://gitlab.com/gitlab-org/gitlab-ee/issues/10216)
* Visualizing job duration [gitlab-ee#2666](https://gitlab.com/gitlab-org/gitlab-ee/issues/2666)

## Top Vision Item(s)

Our most important vision item is the Directed acyclic graphs (DAG) for pipelines MVC [gitlab-ce#47063](https://gitlab.com/gitlab-org/gitlab-ce/issues/47063), which will allow for out of order execution and open a world of new possibilities around how pipelines are constructed.

Additionally, we want to provide better support for our .NET users. Adding support for unit testing frameworks commonly used in those environments will help us become a better solution for everyone. [gitlab-ce#61970](https://gitlab.com/gitlab-org/gitlab-ce/issues/61970) introduces direct support for the TRX file format, avoiding the need for Microsoft shops to have to convert to JUnit.

Another item that is becoming more important is our use of [Docker Machine](https://github.com/docker/machine) for multi-cloud provisioning/autoscaling, which is now in maintenance mode. [gitlab-runner#4338](https://gitlab.com/gitlab-org/gitlab-runner/issues/4338) is where we're discussing what future options would look like.
