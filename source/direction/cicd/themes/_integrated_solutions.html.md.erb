<!-- Introduce theme, stages, and categories it applies to and reference liberally. Highlight
important features, epics and issues from that theme specifically. Concentrate not on what we will do
but also what we won't do over the next 12 months. Themes should include concrete items what we're not going to do. 
Themes need to reference very specific deliverables rather than being abstract concepts. -->

DevOps tooling is heading towards consolidation simply because users prefer well-integrated solutions that don't require administration effort to keep everything wired together in order to realize the benefits of having all the information you need in one place. Point application solutions like TravisCI and CircleCI are struggling under this paradigm because they suffer from the fundamental problem that CI/CD is not an island. In order to make sure their solutions can compete effectively they are forced to maintain integrations with various stacks, which is expensive in terms of focus and results that they are unable to innovate as fast as competitors that are providing a unified solution.

Ultimately, our single application solution is what will help us ensure that GitLab is a [Robot Cat instead of a Sheepdog](https://thenewstack.io/todays-ci-cd-from-fat-sheepdogs-to-robot-cats/). By embedding CI/CD naturally into everyone's workflows, as a first-class part of working with issues, source control, merge requests, etc., CI/CD isn't a box under someone's desk that nobody knows how to work while on vacation; it's a part of how everyone works together - the glue that binds everyone around how they deliver software together as a team. 

Related Epics:

- [Make GitLab CI/CD work better with external repositories](https://gitlab.com/groups/gitlab-org/-/epics/943)
- [Standardize authentication and permissions for the GitLab Registry](https://gitlab.com/groups/gitlab-org/-/epics/1243)
- [Create visibility and transparency for the GitLab Package and Container Registry](https://gitlab.com/groups/gitlab-org/-/epics/1244)
