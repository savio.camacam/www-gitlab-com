---
layout: markdown_page
title: Product Development Flow
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview & philosophy

GitLab's [product mission](/handbook/product/#product-team-mission) is to consistently create products and experiences that users love and value. To deliver on this mission, it's important to have a clearly defined and repeatable flow for turning an idea into something that offers customer value. 

This page describes how we expect our cross-functional development teams to work. Note that this product development flow is aspirational in nature. We will iterate our way towards this system, and as we make changes and improvements, this page will evolve. It's also important to allow open source contributions at any point in the process from the wider GitLab community. 

## Validation track  

For new ideas where the customer problem and solution is not well understood, PMs and UXers should work together to validate new opportunities before moving to the Build track. This **Validation** track should happen as an independent track from the always-moving **Build** track. 

The goal is for PMs and UXers to get 1-2 months ahead, so that the Build track is filled with well-validated product opportunities. Ideally PMs and UXers spend roughly 50% of their time doing validation activities. Validation cycles may not be necessary for things like bug fixes, well understood iterative improvements, minor design fixes, etc.

### Validation phase 1: Validation backlog

1. PM maintains a backlog of potential validation opportunities. Validation opportunities may come from customers, internal stakeholders, product usage insights, support tickets, win/loss data, etc.  Validation opportunities should be captured as an epic and described in customer problem language, rather than in feature/solution language.
1. PM to apply the `~"workflow::validation backlog"` label to the epic. 
1. PM to prioritize the backlog using the [RICE methodology].(https://www.productplan.com/glossary/rice-scoring-model/)  

### Validation phase 2: Problem validation

We believe that good product development starts with a well understood and tightly articulated customer problem. Once we have a clear and shared understanding of the customer problem, then generating solutions, developing the product experience, and launching to the market becomes much more effective. 

The danger in not starting with the problem is that you might miss out on solutions that come from deeply understanding the customer problem. A poorly defined problem statement can also cause the design and development phases to be inefficient.

[Velocity](../engineering/#the-importance-of-velocity) and [iteration](../values/#iteration) are both key competitive attributes of GitLab Inc, but done in isolation are not the most effective or efficient route to [customer results](../values/#customer-results).

#### Problem validation criteria

We expect PM and UX to partner on a problem validation phase when the customer problem to be solved isn't clear.  The following situations often require problem validation:

- Initiating work on a new [product category](categories/)
- Defining the next [maturity state](https://about.gitlab.com/direction/maturity/) for a product category (for example, when researching how to go from `Complete` to `Lovable`)
- The envisioned feature is large or introduces a major change to the user experience (for example, reorganizing the sidebar navigation)
- Targeting a new user or buyer persona

Some items will skip the problem validation phase. In these cases the problem is well understood and has been validated in other ways. When skipping problem validation, ensure the issue description is clear with the rationale and sensing mechanisms used to skip the problem validation phase.

#### Problem validation process description

1. PM to apply the `~"workflow::problem validation"` label to the associated epic.
1. PM to fill out an [opportunity canvas](https://app.mural.co/t/gitlab2474/m/gitlab2474/1560981265035/b3ca2859384a3b16f3b99e2ae392cd0b0bc9b31e) to the best of their ability at this point of the process. Ensure the problem and persona is well articulated at this point in the process.
1. PM to fill out a customer interview guide with a draft list of interview questions. You can find a good guide [here](https://www.nngroup.com/articles/user-interviews/).
1. PM to review customer interview guide and opportunity canvas with UX peers.
1. UX Research will help recruit candidates for interviews. 
1. PM plus UX to conduct at least 5 interviews with target customers. Interview notes should be logged as an issue in the [user interview project](https://gitlab.com/gitlab-com/user-interviews) and labeled with relevant stages. (training video coming soon)
1. PM plus UX synthesize customer feedback using affinity mapping or other techniques. (training video coming soon)
1. PM to fully complete and update the opportunity canvas with the synthesized feedback.
1. PM to schedule a review of the opportunity canvas with Scott Williamson, Christie Lenneville, and the Product Director in your area. Weekly time blocks will be held.  Contact Vanessa Wheeler to get your review added to one of the weekly time blocks.
1. If approved, create required epics and issues and move to the solution validation phase.

### Validation phase 3: Solution validation

Once the customer problem is well understood, now it's time to validate the solution to the problem.   
1. PM to apply the `~"workflow::solution validation"` label to the associated epic.
1. PM and UX co-create a [story map](https://www.jpattonassociates.com/the-new-backlog/) to visualize the user stories required to solve the customer problem.  Lines can be drawn on the story map to indicate which user stories need to be included in each release increment.  
1. UX then creates a prototype (low- or high-fidelity screenshots or an interactive UI prototype) that addresses the user stories required for the first release.  UXers should also participate in design reviews to get feedback from peers as an additional source of input. 
1. UX and PM validate the prototype with at least 5 target users through usability testing interviews. 
1. PM updates the associated story map, opportunity canvas, and epic based on customer feedback.

Solution Validation is also relevant for APIs and other technical features that don't require a UI. Communicate these solutions using artifacts such as API docs, workflow diagrams, etc. Involve your Engineering Managers in creating and reviewing these artifacts to gain a shared understanding of the solution and receive input on feasibility.

## Build track  

Validated opportunities should then be pulled into the Build track, where we develop, launch, and improve them over time.

### Build phase 1: Plan

PM, UX, and Engineering Managers break down the opportunities into well-defined issues. Story mapping is a recommended technique to do this in a rapid and collaborative fashion. The resulting issues should be written by PMs in user-story-style language whenever possible: "As a (who), I want (what), so I can (why/value)." Issues should not only be about feature details, but should also establish functional, performance, and security-acceptance criteria. 

Use the [feature proposal template](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab/issue_templates/Feature%20proposal.md) as a guide for writing both epics and features. Then, size the issues and assign them to a specific milestone/release. Consult with your stable counterparts in a given work specialization to estimate effort. Lastly, follow the typical monthly release process, including kickoff calls.

### Build phase 2: Develop & test

Engineering teams execute on the scheduled work. Acceptance criteria as set forth in the issues must be met before a feature is deemed launchable. Work deemed out of scope or incomplete is taken back into the Plan phase for grooming and rescheduling for completion. PM should conduct feature-level acceptance testing to ensure that the intended customer value was, in fact, delivered.

### Build phase 3: Launch

The Product Marketing Manager and PM should collaborate on messaging and positioning, and the Product Marketing Manager should own a marketing plan describing how the opportunity will be launched into customer hands. The PM will also add relevant content to the release post per the typical monthly process.

### Build phase 4: Improve

The PM must articulate success metrics for each opportunity and ensure product instrumentation and dashboarding are in place at the time of launch. After launch, the PM should pay close attention to product usage data and customer feedback to guide follow-on iterative improvements, until success metrics are achieved or a decision is made that the product experience is good enough.
