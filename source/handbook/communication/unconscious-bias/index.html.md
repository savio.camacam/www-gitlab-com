---
layout: markdown_page
title: "Unconscious bias"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is unconscious bias?

Everyone has unconscious biases, the goal is to bring them to our consciousness and navigate them. These biases help the brain create short cuts to shorten the decision-making process and detect threats. Our unconscious biases are based on our own experiences and they help us detect patterns and find in-groups, a basic survival mechanism below our conscious radar. If unconscious bias goes unchecked, it can lead to fixed general views of how people should act or behave, and/or negative out-spoken attitude towards a person or group.

## How do I recognize it?

**Gain self-awareness and be present** Pay attention to your body language and tone. Instead of reacting, set up a response. Monitor your self-talk.
**Exhibit tolerance and patience; avoid assumptions** Choose to listen, pause, and consider first. Offer the benefit of the doubt. Don't jump to conclusions. Ask questions.
**Make thoughtful impressions and decisions** Challenge your own default views. Take all options into consideration. Use informed decisions, not just your "gut".
**Seek feedback and choose to use it** Don't avoid the conversation, seek it out. Ask for the feedback you need, not want. Achieve by understanding, not defending.
**Remain open-minded and continue learning** Hold yourself and others accountable. Revisit, reflect, and review your day. Diversify your circles.

### Seven Unconscious Biases to look out for (in ourselves and in others)

* **Affinity Bias**: Biased towards people "who make me comfortable"; Biased against people "who make me uncomfortable".
* **Affective Heuristic Bias**: Immediate emotional judgement influenced by superficial traits such as race, gender, age, or names
* **Confirmation Bias**: People accept ideas or findings which confirm their belief.
* **Conformity Bias**: Tendency to take cues for proper behavior in most context based on the actions of others.
* **Halo Effect**: Form of bias which favors one aspect that makes a person seem more attractive or desirable.
* **Horns Effect**: Form of bias that causes one's perception of another to be overly influenced by one or more negative traits.
* **Contrast Effect**: Tendency to promote or demote someone after a single comparison with one or more of their peers.

## Resources
[Ongoing discussion](https://gitlab.com/gitlab-com/diversity-and-inclusion/issues/27)
[This MVC is based on Ashley Sullivan's comment here](https://gitlab.com/gitlab-com/people-ops/General/issues/379#note_208972342)
[Diversity and Inclusion at GitLab](/company/culture/inclusion/)