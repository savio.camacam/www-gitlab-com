---
layout: markdown_page
title: "IR.2.02 - Incident Reporting Contact Information Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IR.2.02 - Incident Reporting Contact Information

## Control Statement

GitLab provides a contact method for external parties to:

* Submit complaints and inquiries
* Report incidents

## Context

Having an easily accessible and public channel for external parties to contact GitLab in the event of a security incident provides a way for the community to help GitLab keep its systems safe and to faster identify and respond to security incidents internally.

## Scope

TBD

## Ownership

TBD

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Incident Reporting Contact Information control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/843).

## Framework Mapping

* ISO
  * A.16.1.2
* SOC2 CC
  * CC2.3
* PCI
  * 12.10.1
