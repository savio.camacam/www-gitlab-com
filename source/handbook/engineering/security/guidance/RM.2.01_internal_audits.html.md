---
layout: markdown_page
title: "RM.2.01 - Internal Audits Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# RM.2.01 - Internal Audits

## Control Statement

GitLab establishes internal audit requirements and executes audits on information systems and processes quarterly.

## Context

Audits are meant to validate processes and check to see if these controls we have implemented are having the desired effect and are performed the way we intended. Internal audits have a bad reputation, but these internal audits help the audit and compliance teams to build the information they need to be the main point of contact with external audits when needed. Successful internal audits can help keep external auditors away from GitLab team-members unless absolutely necessary.

## Scope

Internal audits are performed against all GitLab production systems and all processes that interact with those systems. Internal audits are also performed against all security compliance controls.

## Ownership

* Control Owner: `Internal Audit`
* Process owner(s): 
    * Internal Audit: `100%`

## Guidance

Internal audits should be a process that all GitLabbers feel comfortable being transparent about how the related process is working and what the outcome of that process is. Full transparency in an internal audit can help ensure all processes are effective prior to an external audit.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Internal Audits control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/870).

## Framework Mapping

* ISO
  * A.12.7.1
  * A.18.2.1
  * A.18.2.2
  * A.18.2.3
* SOC2 CC
  * CC1.2
  * CC3.2
  * CC3.4
  * CC4.1
  * CC4.2
  * CC5.1
  * CC5.2
