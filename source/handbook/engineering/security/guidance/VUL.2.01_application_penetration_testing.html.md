---
layout: markdown_page
title: "VUL.2.01 - Application Penetration Testing Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# VUL.2.01 - Application & Infrastructure Penetration Testing

## Control Statement

GitLab conducts penetration tests according to the service risk rating assignment.

## Context

This control is meant to formalize the way GitLab prioritizes our penetration tests. The rating assignment mentioned in this control is detailed in a separate control linked below. It isn't feasible to test 100% of GitLab systems and since penetration tests are meant to reduce risk to the organization, it makes sense that risk is the method we use for prioritizing which systems we test in a given year.

## Scope

This control applies to penetration testing performed against any GitLab production systems.

## Ownership

Control Owner:

* Senior Director of Security

Process Owner:

* Application Security Team - Hacker1 and External 3rd-Party vendor relationship
* Red Team, Security - provide supplemental/enhanced penetration testing
* Infrastructure - Responsible for infrastructure penetration testing

## Guidance

We will need to share our methodology for determining which systems to pen test and that methodology should align with the related control.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Application Penetration Testing control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/939).

## Framework Mapping

* ISO
  * A.12.6.1
* SOC2 CC
  * CC7.1
* PCI
  * 11.3
  * 11.3.1
  * 11.3.2
  * 11.3.4
