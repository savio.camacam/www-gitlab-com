---
layout: markdown_page
title: "SO.2.01 - Provisioning Physical Access Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SO.2.01 - Provisioning Physical Access

## Control Statement

Physical access provisioning to a GitLab datacenter requires management approval and documented specification of:

 * Account type (e.g., standard, visitor, or vendor)
 * Access privileges granted
 * Intended business purpose
 * Visitor identification method, if applicable
 * Temporary badge issued, if applicable
 * Access start date
 * Access duration

## Context

This control refers to physical access to GitLab datacenters.

## Scope

This control is not applicable to GitLab's SaaS product since managed hosting is used for all systems relating to this product. GitLab does not use any datacenters to which any GitLab team-members have any physical access. We rely on the audit certifications for the underlying hosting companies for this control.

## Ownership

* Control Owner: `Security`
* Process owner(s):
    * Security: `70%`
    * Infrastructure: `30%`

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Provisioning Physical Access control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/912).

## Framework Mapping

* ISO
  * A.11.1.2
* SOC2 CC
  CC6.4
* PCI
  * 9.2
  * 9.3
  * 9.4
  * 9.4.1
  * 9.4.2
  * 9.5
