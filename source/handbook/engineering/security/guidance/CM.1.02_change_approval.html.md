---
layout: markdown_page
title: "CM.1.02 - Change Approval Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# CM.1.02 - Change Approval

## Control Statement

Prior to introducing changes into the production environment, approval from authorized personnel is required based on the following:

* Change description
* Impact of change
* Test results
* Back-out procedures

## Context

This control aims to ensure important information about the change, its impacts, and ability to revert the change are documented and a part of the approval process. This allows everyone involved to have the information they need to make informed decisions about and execute on a change effectively. It also sets out to ensure all changes which could impact GitLab customers, GitLab team-members, and partners are approved by the appropriate person(s).

## Scope

This control applies to any application or infrastructure changes introduced into the GitLab production environment.

## Ownership

Control Owner:

* Engineering Group

Process owner(s):

* Infrastructure Team: 50%

* All engineering teams: 50%

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Change Approval control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/782).

## Framework Mapping

* ISO
  * A.12.5.1
  * A.14.2.3
  * A.14.2.4
  * A.14.2.8
  * A.14.2.9
* SOC2 CC
  * CC8.1
* PCI
  * 1.1.1
  * 10.4.2
  * 6.3.2
  * 6.4
  * 6.4.5
  * 6.4.5.1
  * 6.4.5.2
  * 6.4.5.3
  * 6.4.5.4
  * 6.4.6
