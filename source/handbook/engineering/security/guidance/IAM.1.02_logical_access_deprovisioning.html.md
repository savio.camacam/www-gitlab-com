---
layout: markdown_page
title: "IAM.1.02 - Logical Access De-Provisioning Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.1.02 - Logical Access De-Provisioning

## Control Statement

Logical access that is no longer required in the event of a termination is documented, communicated to management, and revoked.

## Context

The purpose of this control is to ensure there is a process in place to remove access to user accounts that is no longer necessary. This control helps ensure that only authorized and active accounts can be accessed and used to prevent any unauthorized use or access of GitLab customer, GitLab teammember, and partner data.

## Scope

This control applies to any system or service where user accounts can be provisioned.

## Ownership

* Control Owner: `IT Ops`
* Process owner(s):
    * IT Ops: `20%`
    * Infrastructure: `20%`
    * System Owners: `20%`
    * People Ops: `20%`
    * Security: `20%`

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Logical Access De-Provisioning control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/806).

## Framework Mapping

* ISO
  * A.7.3.1
  * A.9.2.1
  * A.9.2.2
  * A.9.2.3
  * A.9.4.1
  * A.9.2.6
  * A.18.1.3
* SOC2 CC
  * CC6.2
  * CC6.3
  * CC6.6
  * CC6.7
* PCI
  * 8.1.2
  * 8.1.3
  * 8.1.4
