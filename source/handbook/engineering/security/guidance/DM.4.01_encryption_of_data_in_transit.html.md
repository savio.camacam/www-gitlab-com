---
layout: markdown_page
title: "DM.4.01 - Encryption of Data in Transit Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# DM.4.01 - Encryption of Data in Transit

## Control Statement

GitLab encrypts red, orange, and yellow data transmitted over public networks.

## Context

Encrypting data transmitted over public networks helps ensure the confidentiality and integrity of that data. Without encryption, data in transit over public networks can easily be intercepted using automated, open source tools and viewed and maliciously modified by malicious actors.

## Scope

Encrypting data in transit over public networks applies to all red, orange, and yellow data.

## Ownership

TBD

## Guidance

TLS 1.2 or higher should be used to encrypt data in transit ([Deprecate support for TLS 1.0 and TLS 1.1](https://gitlab.com/gitlab-com/gl-security/engineering/issues/202)).

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Encryption of Data in Transit control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/796).

## Framework Mapping

* ISO
  * A.13.2.3
  * A.14.1.2
  * A.14.1.3
  * A.18.1.4
  * A.18.1.5
* SOC2 CC
  * CC6.7
* PCI
  * 2.3
  * 4.1
  * 4.1.1
  * 8.2.1
