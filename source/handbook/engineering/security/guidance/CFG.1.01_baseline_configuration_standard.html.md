---
layout: markdown_page
title: "CFG.1.01 - Baseline Configuration Standard Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# CFG.1.01 - Baseline Configuration Standard

## Control Statement

GitLab ensures security hardening and baseline configuration standards have been established according to industry standards and are reviewed and updated quarterly.

## Context

Baseline hardening standards make it clear how systems should be hardened and configured.  To ensure we these standards are always relevant, we need to regularly review these documents and update them as needed.  The goal of this control is to remove as much subjectivity as possible from the process of configuring systems.  If we create a standard for each system type within GitLab, it will be easier to automate system configuration and ensure that all systems are configured the same.  This consistent configuration becomes critical when critical vulnerabilities are discovered and need to be rapidly deployed to all applicable systems.

## Scope

This control applies to all hosted systems (e.g. VM's and GCP compute services) as well as end user workstations (e.g. GitLab team-members' MacBooks).

## Ownership

* Control Owner: `Security`
* Process owner(s):
    * Security: `50%`
    * IT Ops: `25%`
    * Infrastructure: `25%`
    
## Guidance

We do not have to reinvent the wheel with these, whenever possible we should be referencing industry standards for system configurations (e.g. NIST guidelines)

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Baseline Configuration Standard control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/784).

## Framework Mapping

* ISO
  * A.12.5.1
* SOC2 CC
  * CC7.1
  * CC7.2
* PCI
  * 1.1
  * 1.1.4
  * 1.1.6
  * 1.2
  * 1.2.2
  * 2.1
  * 2.1.1
  * 2.2
  * 2.2.2
  * 2.2.3
  * 2.2.4
  * 2.2.5
  * 5.3
