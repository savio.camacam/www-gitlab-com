---
layout: markdown_page
title: "Contributing to Development"
---

## Development 

These instructions are for development of GitLab Community Edition (CE), but most are also applicable to other GitLab projects. Please note that use of the GitLab Development Kit is currently experimental on Windows. Linux or macOS are recommended for the best contribution experience.

1. Download and set up the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit), see the [GDK README](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/README.md) for instructions on setting it up and [Troubleshooting](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/troubleshooting.md) if you get stuck.
1. [Fork the GitLab CE project](https://gitlab.com/gitlab-org/gitlab-ce/-/forks/new).
1. Choose an issue to work on.
    - You can find easy issues by [looking at issues labeled `Accepting merge requests` sorted by weight](https://gitlab.com/groups/gitlab-org/-/issues?assignee_id=None&label_name%5B%5D=Accepting+merge+requests&scope=all&sort=weight&state=opened&utf8=%E2%9C%93). Low-weight issues will be the easiest to accomplish.
    - Be sure to comment and verify no one else is working on the issue, and to make sure we’re still interested in a given contribution.
    - You may also want to comment and ask for help if you’re new or if you get stuck. We’re more than happy to help!
1. Add the feature or fix the bug you’ve chosen to work on.
1. If it's a feature change that impacts users or admins, [update the documentation](https://docs.gitlab.com/ee/development/documentation/).
1. [Open a merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) to merge your code and its documentation. The earlier you open a merge request, the sooner you can get feedback. You can [mark it as a Work in Progress](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html) to signal that you’re not done yet. If you're including documentation changes or need help with documentation, mention `@gl-docsteam`.
1. Add tests if needed, as well as [a changelog entry](https://docs.gitlab.com/ee/development/changelog.html).
1. Make sure the test suite is passing.
1. Wait for a reviewer. You’ll likely need to change some things once the reviewer has completed a code review for your merge request. You may also need multiple reviews depending on the size of the change. If you don't hear from anyone in a timely manner, feel free find reviewers or reach out to [Merge Request Coaches](/job-families/expert/merge-request-coach/) as mentioned in [the getting help section](/community/contribute/#getting-help).  
1. Get your changes merged!

For more information, please see the [Developer Documentation](https://docs.gitlab.com/ee/development/README.html).
