---
layout: job_family_page
title: "Senior Director of Product"
---

As the Senior Director of Product, you will play a key role in helping the GitLab Product Management team scale rapidly and realize our product vision to be a complete DevOps platform.

## Responsibilities
- Hire, lead, and coach a rapidly growing team of 20+ Product Managers
- Directly manage a team of 4-6 Product Directors
- Ensure a cohesive, coherent, and compelling end-to-end customer experience
- Partner effectively with Engineering, Design, and Product Marketing to ensure we validate, build, launch, and measure product experiences that customers love and value
- Help refine and implement the GitLab [product development flow](https://about.gitlab.com/handbook/product-development-flow/), ensuring team members receive training and coaching required to work effectively within the system
- Serve as a spokesperson for the end-to-end GitLab product internally and externally

## Requirements
- 12 years+ of Product Management experience, with 6+ years of people management experience
- Strong understanding of DevOps markets, competition, and underlying technologies
- Track record of leading products to successful commercial outcomes
- Demonstrated ability to teach and coach the product management skills as outlined [here](https://about.gitlab.com/handbook/product/#product-management-career-development-framework)
