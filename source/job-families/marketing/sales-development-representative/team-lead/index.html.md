---
layout: job_family_page
title: "Sales Development Representative Team Lead"
---
## Job Description

The Sales Development Representative (SDR) Team lead is a step in the career path geared towards SDRs getting exposed to what it’s like to manage a team at GitLab. You’ll need to have stellar organizational and time management skills because you will be taking point on various SDR leadership tasks such as: on-boarding new employees, managing event followup, interviewing, etc. all while continuing to exceed your quota targets. The SDR Team Lead will also be included in some management level meetings. This concept is to help foster those who have expressed interest in leading a team and progressing to management within GitLab.

### Responsibilities

* Will be assigned as a Dedicated Onboarding Buddy
* Assists in interviewing process
* Leads team role play and SDR weekly team calls
* Covers when SDR's are out of office (OOO)

### Requirements

* 18 months of SDR experience (six (6) months at GitLab)
* Proficient in the use of Command of Message (CoM) framework and SDR tools
* In-depth product knowledge
* Stellar time management and organizational skills
* Is seen as a leader/coach by peers as well as the SDR leadership team