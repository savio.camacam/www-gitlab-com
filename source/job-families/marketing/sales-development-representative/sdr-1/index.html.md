---
layout: job_family_page
title: "Sales Development Representative 1"
---
## Job Description

For newcomers to the Sales Development Representative (SDR) 1 function at GitLab, the SDR 1 level is designed to make you proficient with SaaS and Open Core sales skills. For a minimum of six (6) months, your focus will be to refine your spoken and written communication skills to be on par with GitLab’s Sales methodology, tone of voice and messaging. You will also spend time improving your business acumen and adjusting your GitLab pitch to different business use cases and following the Command of the Message (CoM) methodology. Upon reaching target goals and acquiring certification on these skills, the next step is an [SDR 2](/job-families/marketing/sales-development-representative/sdr-2) role.

### Responsibilities

* Meet or exceed [Sales Development Representative (SDR)](/job-families/marketing/sales-development-representative) sourced Sales Accepted Opportunity (SAO) volume targets.
* Identify and create new qualified opportunities from inbound MQLs (Marketing Qualified Lead)
* Be able to identify where a prospect is in the sales and marketing funnel and nurture appropriately
* Participate in documenting all processes in the handbook and update as needed with your Manager
* Be accountable for your lead data and prospecting activities, log everything, take notes early and often
* Enhance prospective customer GitLab trial experiences through tailored and timely outreach
* Engage in conversations with prospective GitLab customers via live chat
* Speed to lead: maintain one-day SLA for all contact requests
* Discretion to qualify and disqualify leads when appropriate
* Develop and execute nurture sequences
* Work with designated Strategic Account Leaders (SAL) to identify and prioritize key accounts to develop, identify net-new opportunities and nurture existing opportunities within target accounts.
* Work in collaboration with Field and Corporate marketing to drive attendance at events

### Requirements

* Twelve (12) months minimum experience in sales or client success preferably in a software/tech company
* Excellent spoken and written English
* Experience with CRM and/or marketing automation software
* An understanding of B2B software sales, open-source software, and the developer product space
* Ability to actively listen
* Be ready to learn how to use GitLab and Git
* You share our values and work in accordance with those values
