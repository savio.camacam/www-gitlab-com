---
layout: job_family_page
title: "Chief of Staff"
---

The Chief of Staff is a very talented individual who serves as a trusted advisor and confidant to the Chief Executive Officer (CEO) for up to 18 months. The Chief of Staff and the CEO work closely together to get twice as much done as the CEO would be able to do alone. Over time the work will get more interesting culminating in a senior leadership position at GitLab or another company. This is a unique opportunity to see every aspect of a fast-growing unicorn at the CEO level. For more context see the [references](#references) at the end of this page.

This is a senior leadership role. This role does not do any work our [Executive Assistants](/job-families/people-ops/executive-assistant/) are already doing, who for example, is responsible for scheduling and driving the [Objective and Key Results](/company/okrs/) (OKR) process. It also is unrelated to the work of our [CEO Shadow training program](/handbook/ceo/shadow/) who, for example, takes notes during all meetings with the CEO.

## [Chief of Staff Q&A Video with the CEO](https://youtu.be/uUwmlJfim6U)

## Responsibilities

The responsibilities include both directed work that will mostly be triggered by a direction from the CEO as well as leadership work that is self-directed. Over the course of a rotation, the mix will shift from majority directed work to majority leadership work.

### Directed Work

1. Create investor updates, [group conversations](/handbook/people-operations/group-conversations/), pitch decks, speeches, conference submissions, and presentations on behalf of the CEO.
1. Suggest changes to processes via [the handbook](/handbook/handbook-usage/) based on discussions during meetings or from the CEO and address any comments on them.
1. Draft blog posts and articles out of recorded meetings and handbook content.
1. Ensure all upcoming meetings are relevant, well structured, and that the preparation is complete and correct.
1. Ensure action points coming out of meetings (i.e. executive, OKR, metrics, or incident meetings) are implemented.
1. Reinforce [communication guidelines](/handbook/communication/). 
1. Help create [mecefu-terms](/handbook/communication/#mecefu-terms).
1. Moderate the [e-group](/handbook/leadership/#e-group) meetings.
1. Keep this job family up-to-date.

### Leadership Work

1. Initiate and guide the OKR process according to the [OKR schedule](/company/okrs/#schedule)
1. Review OKR suggestions during the [scheduled OKR meetings](/company/okrs/#schedule) and outside of the meetings.
1. Brief CEO visitors on GitLab before they visit.
1. Help contribute topics for the [E-group offsite](/handbook/ceo/offsite/).
1. Serve as a proxy for the CEO by attending meetings in place of the CEO and making decisions on the CEO's behalf.
1. Chief of Staff will participate in every E-group meeting. The person will not be an executive or part of the e-group.
1. Act as the communication arm for the CEO with team members, clients, investors, community members, and other stakeholders.
1. Act as a sounding board to the CEO for new ideas and initiatives.
1. Run special projects that are urgent and important, for example building a company wide [compensation model](/handbook/people-operations/global-compensation/#compensation-calculator).
   
## Requirements

1. Capacity to become a [senior leader](/company/team/structure/#senior-leaders) at GitLab.
1. Proven ability to quickly learn new things.
1. Concise written and verbal communication.
1. Based in San Francisco, CA, USA to attend in-person meetings.
1. Structured about how to prioritize work and time.
1. Able to make a clear business case for proposals.
1. Master’s degree in Engineering or Business Administration or equivalent experience.
1. Experience with data analysis.
1. Proven success in project coordination and reporting.
1. Proven track record of effectively interacting with senior stakeholders on cross-functional projects.
1. Experience leading a team.
1. 4+ years combined experience in:
   - Senior operating role at a significant fast-growing startup,
   - Strategy consulting,
   - or Venture capital.
1. Operational experience in a previous role

### Preferred Requirements

1. Knows how to use git.

## Rotation
Rotation is approximately 15 to 18 months with the following tentative schedule:

   - 2 months of onboarding, 
   - 10 months of execution,
   - 1 - 4 months of recruiting your replacement and waiting for them to start,
   - 2 - 3 months of onboarding your replacement and interviewing at GitLab for senior leadership positions.
   
## Internal Consultants
The Chief of Staff could have two high potential but relatively inexperienced operators report to them; i.e. someone who left a strategy consultancy after a few years. These internal consultants are training for future functions in the company. Their responsibilities could include; analysis, middle management and fill in the experience gaps missing from the Chief of Staff.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

   * Qualified candidates will be invited to schedule a 30 minute screening call with a Recruiting Manager
   * Then, candidates will be invited to schedule a 50 minute interview with the Internal Strategy Consultant and a 50 minute interview with the Engineering Manager, Delivery (employee number three)
   * Next, candidates will be invited to schedule a 90 minute interview with the CEO
   * Next, candidates will be invited to schedule a 90 minute interview with the Chief Revenue Officer and a 50 minute interview with the VP of Engineering
   * Finally, candidates will be invited to a 50 minute interview with the CEO's Mentor

As always, the interviews and screening call will be conducted via a [video call](/handbook/communication/#video-calls). See more details about our interview process [here](/handbook/hiring/interviewing/).

## References
* [What Is a Chief of Staff and When Do You Need One?](https://www.bridgespan.org/insights/library/careers/chief-of-staff-role#sidebar-two) by Madeleine Niebauer 2018-09-26
* [The Chief of Staff role in Silicon Valley](https://medium.com/@juliadewahl/the-chief-of-staff-role-in-silicon-valley-182eb93e636e) by Julia DeWahl 2019-04-29
