---
layout: job_family_page
title: "Candidate Experience Specialist"
---

## Candidate Experience Specialist

The GitLab Candidate Experience Specialists works to create positive experiences for GitLab candidates and hiring teams. In a growing fast-paced environment, the Candidate Experience Specialist is a dynamic team member who helps create an amazing candidate experience, improve our existing hiring practices, and deliver exceptional results to our hiring teams. GitLab strives to be a preferred employer and relies on the Candidate Experience Specialists to act as brand ambassadors by embodying the company values during every interaction with potential team members. The Candidate Experience Specialist is enthusiastic about working with high volume and is dedicated to helping GitLab build a qualified, diverse, and motivated team. The Candidate Experience Specialist reports to the Candidate Experience Manager.

### Responsibilities

* Collaborate with Recruiters and Recruiting Manager to understand the process for each open vacancy
* Work in Applicant Tracking System (ATS) to help recruiters maintain a positive candidate experience for candidates
* Helps manage candidate traffic for all roles
* Open and close vacancies through GitLab with the coordination of ATS
* Act as the Recruiting Coordinator for the interview process
* Assist with conducting reference checks for all candidates entering the final round of interview phase
* Build an effective network of internal and external resources to call on as needed
* Ensure candidates receive timely, thoughtful and engaging messaging throughout the hiring process
* Assist Recruiting team with putting together GitLab hiring packages and corresponding with hiring managers and CEO for proper approvals
* Create and stage offer letter and employment contracts for recruiting team
* Assist with Onboarding processes, including ordering equipment and supplies for team members
* Promote our values, culture and remote only passion

### Requirements

* Desire to learn about Talent Acquisition and Human Resources from the ground level up
* Demonstrated ability to work in a team environment and work with C-Suite level candidates Focus on delivering an excellent candidate experience
* Ambitious, efficient and stable under tight deadlines and competing priorities
* Proficient in Google Suite
* Ability to build relationships with managers and colleagues across multiple disciplines and timezones
* Outstanding written and verbal communication skills across all levels
* Willingness to learn and use software tools including Git and GitLab
* Organized, efficient, and proactive with a keen sense of urgency
* Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).
* Excellent communication and interpersonal skills
* Prior experience using an candidate tracking system, Greenhouse experience is a plus
* Ability to recognize and appropriately handle highly sensitive and confidential information
* Experience working remotely is a plus
* You share our [values](/handbook/values), and work in accordance with those values.

## Performance Indicators

*   [Average candidate ASAT](https://about.gitlab.com/handbook/hiring/metrics/#candidate-asat)
*   [Hires vs. Plan](https://about.gitlab.com/handbook/hiring/metrics/#hires-vs-plan)
*   [Apply to Offer Accept](https://about.gitlab.com/handbook/hiring/metrics/#apply-to-offer-accept-days)
*   [CES Service Desk Issues Response Time and Issue Distribution](https://about.gitlab.com/handbook/hiring/metrics/#ces-service-desk-metrics)
*   [CES Response Time](https://about.gitlab.com/handbook/hiring/metrics/#ces-response-time)
*   [Candidate Time Per Stage](https://about.gitlab.com/handbook/hiring/metrics/#candidate-time-per-stage)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team/).

   * Qualified candidates will be invited to schedule a 30 minute screening call with a Recruiting Manager
   * Then, candidates will be invited to schedule two 30 minute interviews with two separate Peers and a 30 minute interview with another Recruiting Manager
   * Finally, candidates will be invited to a 45 minute interview with the Hiring Manager

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).
