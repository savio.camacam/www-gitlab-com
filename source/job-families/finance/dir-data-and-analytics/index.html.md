---
layout: job_family_page
title: "Director of Data and Analytics"
---

 The director of data and analytics is an organziational leader with a passion for data and analysis who has a clear vision for how data can transform company strategy. The role requires hands on leadership necessary to grow a team through the start up phase into a mature organization. The director of data and analytics is responsible for scaling the data function in an environment based primarily on cloud based SaaS systems.  The director must consider global operations and be able to manage a remote, geographically dispersed team. The director ensures that the organization always approaches work with the goal of continuous improvement.

### Responsibilities
1. Drive the scope and effectiveness of the data and analysis function at GitLab.
1. Ensure the Company’s cloud and on-premise data is centralized into a single data lake and modeled to support data analysis requirements from all functional groups of the Company.
1. Create a common data framework so that all company data can be analyzed in a unified manner.
1. Work with the product, operations, and executive management teams to create a data enabled user journey.
1. Create and execute a plan to develop and mature our ability to measure and optimize usage growth, mapped to our user journey.
1. Ensure that all transactional systems can communicate with the data warehouse and that production data adheres to a unified data model.
1. Develop a roadmap for the data and analytics function that clearly defines ownership and responsibility between the central data function and the functional groups.
1. Collaborate with all functions of the company to ensure data needs are addressed and the required data is modeled and available to analysts and end-users.
1. Build a multi-modal service model that meets the non-homogeneous needs of our functional groups -- Full-service to Self-Service, and across our data stack.
1. Work with product, operations and executive management to guide maintain a holistic vision of the future of data at GitLab, and help leadership plan for any changes in our data strategy or needs. An example is in-product analytics.
1. This position reports directly to the CFO and works closely with the executive team to develop an organization plan that addresses company wide analytic resources in either a direct report or matrix model.

### Requirements
1. Postgraduate work or equivalent experience (Masters or PhD) in a quantitative field such as math, physics, computer science, statistics etc.
1. Minimum of 7 years experience in a senior leadership position managing an analytics team. 
1. Experience with a high growth company using on-premise tools and on-demand (SaaS) transactional systems.
1. Hands on experience with Python, SQL and relational databases. Experience with Snowflake is a plus.
1. Have previously lead a corporate data platform project.
1. Experience with open source data & analytics tools.
1. Experience working with multiple executive level business stake holders.
1. Must have experience with analytic and data visualization tools such as Periscope.
1. Share and work in accordance with our values.
1. Must be able to work in alignment with Americas timezones.
1. Successful completion of a background check.

## PIs
*  [New Hire Location Factor < 0.69](/handbook/business-ops/metrics/)
*  [% of team who self-classify as diverse](/handbook/business-ops/metrics/)
*  [Discretionary bonus per employee per month > 0.1](/handbook/business-ops/metrics/)
*  [Cost Actual vs Plan](/handbook/business-ops/metrics/)
*  [% of company data in data lake](/handbook/business-ops/metrics/)
*  [New Data or Functionality every 3 months](/handbook/business-ops/metrics/)

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.
1. Selected candidates will be invited to schedule a screening call with our Global Recruiters
1. Next, candidates will be invited to schedule a first interview with our CFO
1. Candidates will then be invited to schedule a second round of interviews with members of the e-group, finance and data teams.
Additional details about our process can be found on our hiring page.